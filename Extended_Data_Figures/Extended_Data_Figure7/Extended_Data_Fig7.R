##### Extended Data Figure7 - Mutational signatures in esophageal squamous cell carcinoma from eight countries with varying incidence

#Load Libraries
library(tidyverse)
library(reshape2)
library(viridis)
library(cowplot)
library(ggpubr)
options(stringsAsFactors = F)

#Load Data
data <- read.csv("Extended_Data_Fig7_Data.csv")

#Plot SBS16 by Brazil
data$SBS16_group_by_brazil = ifelse(data$country == "Brazil", "Brazil", "Other")

p1 <- ggplot(data,aes(y=SBS16, x=SBS16_group_by_brazil, fill = SBS16_group_by_brazil)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("SBS16") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  stat_compare_means(label.x= 1.25, label.y = max(data$SBS16) + 50)

#Plot SBS16 by Japan
data$SBS16_group_by_japan = ifelse(data$country == "Japan", "Japan", "Other")

#NOTE P value manually defined as defaults will not plot values lower than 2.2e-16
p2_p_value <- wilcox.test(data$SBS16 ~ data$SBS16_group_by_japan)$p.value
p2_p_value <- format(p2_p_value, digits = 2)

p2 <- ggplot(data,aes(y=SBS16, x=SBS16_group_by_japan, fill = SBS16_group_by_japan)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("SBS16") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  annotate("text", x= 1.5, y = max(data$SBS16) + 200, label = paste0("Wilcoxon, p =  ",p2_p_value))

#Plot DBS4 by Brazil
data$DBS4_group_by_brazil = ifelse(data$country == "Brazil", "Brazil", "Other")

p3 <- ggplot(data,aes(y=DBS4, x=DBS4_group_by_brazil, fill = DBS4_group_by_brazil)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("DBS4") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  stat_compare_means(label.x= 1.25, label.y = max(data$DBS4) + 25)

#Plot DBS4 by Japan
data$DBS4_group_by_japan = ifelse(data$country == "Japan", "Japan", "Other")

p4 <- ggplot(data,aes(y=DBS4, x=DBS4_group_by_japan, fill = DBS4_group_by_japan)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("DBS4") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  stat_compare_means(label.x= 1.25, label.y = max(data$DBS4) + 25)

#Plot SBS1 by Iran
data$SBS1_group_by_Iran = ifelse(data$country == "Iran", "Iran", "Other")

#NOTE P value manually defined as defaults will not plot values lower than 2.2e-16
p5_p_value <- wilcox.test(data$SBS1 ~ data$SBS1_group_by_Iran)$p.value
p5_p_value <- format(p5_p_value, digits = 2)

p5 <- ggplot(data,aes(y=SBS1, x=SBS1_group_by_Iran, fill = SBS1_group_by_Iran)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("SBS1") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  annotate("text", x= 1.5, y = max(data$SBS1) + 200, label = paste0("Wilcoxon, p =  ",p5_p_value))

#Plot ID2 by Iran
data$ID2_group_by_Iran = ifelse(data$country == "Iran", "Iran", "Other")

p6 <- ggplot(data,aes(y=ID2, x=ID2_group_by_Iran, fill = ID2_group_by_Iran)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  coord_cartesian(ylim=c(0, 1000)) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("ID2") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  stat_compare_means(label.x= 1.25, label.y = 1000)


#Plot ID11 by Brazil
data$ID11_group_by_brazil = ifelse(data$country == "Brazil", "Brazil", "Other")

p7_p_value <- wilcox.test(data$ID11 ~ data$ID11_group_by_brazil)$p.value
p7_p_value <- format(p7_p_value, digits = 3)

p7 <- ggplot(data,aes(y=ID11, x=ID11_group_by_brazil, fill = ID11_group_by_brazil)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("ID11") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  annotate("text", x= 1.5, y = max(data$ID11) + 50, label = paste0("Wilcoxon, p =  ",p7_p_value))

#Plot ID11 by Japan

data$ID11_group_by_japan = ifelse(data$country == "Japan", "Japan", "Other")

#NOTE P value manually defined as defaults will not plot values lower than 2.2e-16
p8_p_value <- wilcox.test(data$ID11 ~ data$ID11_group_by_japan)$p.value
p8_p_value <- format(p8_p_value, digits = 2)

p8 <- ggplot(data,aes(y=ID11, x=ID11_group_by_japan, fill = ID11_group_by_japan)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  theme_bw() + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Mutation Burden") + 
  ggtitle("ID11") +
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.text.x = element_text(size =12)) +
  theme(axis.text.y = element_text(size =12)) +
  theme(axis.text.x=element_text(angle=45,hjust=1)) +
  theme(axis.title.y = element_text(size =12)) +
  theme(axis.title.x = element_blank()) +
  theme(legend.position = "none") +
  annotate("text", x= 1.5, y = max(data$ID11) + 50, label = paste0("Wilcoxon, p =  ",p8_p_value))

#Plot final file
pdf(file="Extended_Data_Fig7.pdf", width = 15, height = 7.5, useDingbats = FALSE)
plot_grid(p1, p2, p3, p4, p5, p6, p7, p8, nrow = 2, align =  "hv")
dev.off()
