##### Extended Data Figure9 - Mutational signatures in esophageal squamous cell carcinoma from eight countries with varying incidence

#Load Libraries
library(tidyverse)
library(cowplot)
library(reshape2)
library(ggpubr)
library(viridis)
filter = dplyr::filter 

#Load Data - VAF
data <- read.csv("Extended_Data_Fig9_Data.csv")

#Load Data - Attributions
sbs_data <- read.csv(file = "Extended_Data_Fig9_Data_SP.csv", header = TRUE)
sbs_data$APOBEC <- sbs_data$SBS2 + sbs_data$SBS13
sbs_data <- cbind(sbs_data[,c(1,30)])

#merge and remove cases without APOBEC
all_data <- merge(data, sbs_data, by.x = "sample_id", by.y = "Sample")
all_data_filtered <- filter(all_data, APOBEC > 0)

#remove APOBEC and rename columns for pretty plotting
all_data_filtered <- cbind(all_data_filtered[,c(1:4)])
colnames(all_data_filtered) = c("Sample","TCN", 'Other','country')

all_data_filtered_melted <- melt(all_data_filtered)

#Plot Panel a
p1 <- ggplot(all_data_filtered_melted, aes(x = variable, y = value, fill= variable)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  scale_y_continuous(limits = c(0,0.55)) +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  ggtitle("TCN vs Other Contexts") +
  labs(y="Median VAF") + 
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.title = element_text(size = 12)) +
  theme(axis.text = element_text(size = 12)) +
  theme(legend.position = "none") +
  labs(x="Context") 

#Add VAF difference
all_data_filtered$VAF_Diff_Other = all_data_filtered$`TCN` - all_data_filtered$`Other`

#Plot Panel b
p2 <- ggplot(all_data_filtered, aes(x=VAF_Diff_Other)) +
  geom_density(fill = 'grey', adjust = 2) +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  scale_x_continuous(limits = c(-0.1,0.1)) +
  labs(x="Median VAF Difference") +
  ggtitle(" ") +
  geom_vline(aes(xintercept=median(VAF_Diff_Other)), color = 'black', linetype = "dashed") 

#Remove UK cases (n=2) before plotting by country
all_data_filtered_melted <- filter(all_data_filtered_melted, country != "UK")

#Set up factor levels for Country
all_data_filtered_melted$country <- factor(all_data_filtered_melted$country, levels = c("Brazil", "Japan","Tanzania", "Kenya","Malawi", "Iran","China"))


#Plot Panel c
p3 <- ggplot(all_data_filtered_melted, aes(x = variable, y = value, fill= variable)) +
  geom_boxplot() + 
  scale_fill_viridis(discrete = TRUE) +
  scale_y_continuous(limits = c(0,0.55)) +
  facet_grid( ~ country) +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  labs(y="Median VAF") + 
  theme(plot.title = element_text(face="bold", size = 12)) +
  theme(axis.title = element_text(size = 12)) +
  theme(axis.text = element_text(size = 12)) +
  theme(legend.position = "none") +
  labs(x="Context") 

#Make Combined Plot
top_row <- plot_grid(p1,p2, align = 'hv', axis = 'b')
pdf(file="Extended_Data_Fig9.pdf", height = 8, width =10)
plot_grid(top_row, p3, nrow = 2)
dev.off()
  
  
