# Mutographs ESCC code

Scripts used for Mutographs ESCC downstream analysis.

## Mutographs ESCC mutation matrices
Input matrices used for mutational signature analysis are available in [input_mutation_matrices/](input_mutation_matrices/) subfolder. These were generated from consensus calls from CaVEMan and Strelka2 (SNVs) or Pindel and Strelka2 (Indels). These matrices can be used to extract and attribute signatures using [SigProfilerExtractor](https://github.com/AlexandrovLab/SigProfilerExtractor) and [MSA](https://gitlab.com/s.senkin/MSA/) tools. Attributed signatures and metadata available on [EGA](https://ega-archive.org/datasets/EGAD00001006732) can then be used to reproduce the downstream analysis.

## Data availability
Whole genome sequencing data and patient metadata are deposited in the European Genome 715 Phenome Archive (EGA) associated with study [EGAS00001002725](https://ega-archive.org/studies/EGAS00001002725). BAM files for all cases included in the final analysis were deposited in dataset [EGAD00001006868](https://ega-archive.org/datasets/EGAD00001006868), and patient metadata in dataset [EGAD00001006732](https://ega-archive.org/datasets/EGAD00001006732). All other data is provided in the accompanying Supplementary Tables of the ESCC manuscript.

## Figures and extended figures
The plotting scripts for figures are implemented in R version 3.6.3, in the corresponding subfolders:
[Figures/](Figures/)
[Extended_Data_Figures/](Extended_Data_Figures/)

## Epidemiological factors regressions

The main script is implemented in the following Jupyter Python notebook:
```
risk_factor_regressions.ipynb
```

Additional regional analysis of the Iranian opitum signature, based on regional de-novo extraction:

```
regional_Iran_opium_regressions.ipynb
```

## Germline variants regressions

The script is implemented in the following R script:

```
germline_variants_regressions.R
```


# Mutographs Germline calling

The recipe below contains the basic commands used to generate annotated germline VCFs.

## snpEff

#### Extract reads that PASS the Strelka QC

```
zcat genome.vcf.gz | java -jar ~/bin/snpeff/SnpSift.jar filter " (FILTER = 'PASS')"  > clean.vcf
```

### Annotate to dbsnp_138

```
java -jar SnpSift.jar annotate -id dbsnp_138.hg19.vcf variants.vcf.gz > test_run.vcf
```

### snpEff annotations

```
java -Xmx32g -jar ~/bin/snpeff/snpEff.jar hg19 clean.vcf > clean.ann.vcf
```

### Creation of snpEff's high impact VCFs and removal of structural interaction variants

```
cat clean.ann.vcf  | java -jar ~/bin/snpeff/SnpSift.jar filter "(ANN[0].IMPACT = 'HIGH') & (ANN[0].EFFECT !='structural_interaction_variant')   " > test.high.vcf
```

### Creation of missense VCFs

cat test.ann.vcf  | java -jar ~/bin/snpeff/SnpSift.jar filter "(ANN[0].EFFECT = 'missense_variant')   " > test.miss.vcf

### Annotation to dbNSFP v2.9 for high impact variants and missense (done the same for each)

```
java -jar ~/bin/snpeff/SnpSift.jar dbnsfp -v -db /scratch/atkinsj/dbNSFP2.9.txt.gz -f SIFT_pred,Polyphen2_HDIV_pred,Polyphen2_HVAR_pred,MetaLR_pred,ExAC_AC,ExAC_AF,clinvar_clnsig,clinvar_trait,hg38_pos test.high.vcf > test.high.dbsnfp2.9.vcf
```

### Conversion to text file for easy presentation of variants (done the same for each)

```
java -jar ~/bin/snpeff/SnpSift.jar dbnsfp -v -db /scratch/atkinsj/dbNSFP2.9.txt.gz -f SIFT_pred,Polyphen2_HDIV_pred,Polyphen2_HVAR_pred,MetaLR_pred,ExAC_AC,ExAC_AF,clinvar_clnsig,clinvar_trait,hg38_pos test.high.vcf > test.high.dbsnfp2.9.vcf
```

## *Once in the text file format, genes and variants of interest were extracted via grep*
