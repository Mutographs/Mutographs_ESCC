# Mutographs ESCC mutation matrices

Input matrices used for mutational signature analysis, generated from consensus calls from CaVEMan and Strelka2 (SNVs), Pindel and Strelka2 (Indels), or BRASS(Structural Rearrangements).
